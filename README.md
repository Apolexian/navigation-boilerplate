# Android authentication boilerplate

When trying out new android features or idea I often find that I want to have some sort of authentication logic in the app. This usually leads to writing a lot of the same boilerplate code. This project serves as a starting point for a basic authentication flow.

## Getting started

Download the code or clone the repo and run the app. You should be presented with this authentication flow:

![demo](assets/gif/demo.gif)

## Customization

To customize the:

* strings - `res/values/strings.xml`
* drawables - `res/drawable/`
* layouts - `res/layout`
* colors - `res/values/colors.xml`
* styles - `res/values/styles.xml`

## Further

This boilerplate code can then be hooked up to a database or inserted in any other control flow or navigation map.

## Contact

If you need to contact me you can do so via my email on my github profile.
